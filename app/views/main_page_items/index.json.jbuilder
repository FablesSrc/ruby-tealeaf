json.array!(@main_page_items) do |main_page_item|
  json.extract! main_page_item, :id, :title, :description
  json.url main_page_item_url(main_page_item, format: :json)
end
