# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary server in each group
# is considered to be the first unless any hosts have the primary
# property set.  Don't declare `role :all`, it's a meta role.

set :deploy_server, "phosphorus.locum.ru"

# Не включать в поставку разработческие инструменты и пакеты тестирования.
# set :bundle_without,  [:development, :test]

set :user, "hosting_mmailm"
set :login, "mmailm"
set :deploy_to, "/home/#{fetch(:user)}/projects/#{fetch(:application)}"
set :unicorn_conf, "/etc/unicorn/#{fetch(:application)}.#{fetch(:login)}.rb"
set :unicorn_pid, "/var/run/unicorn/#{fetch(:user)}/#{fetch(:application)}.#{fetch(:login)}.pid"

server fetch(:deploy_server), user: fetch(:user), roles: %w{web app db}

set :bundle_path, '../../vendor/gems'

set :unicorn_start_cmd, "(cd #{fetch(:deploy_to)}/current; rvm use #{fetch(:rvm_ruby_version)} do bundle exec unicorn_rails -Dc #{fetch(:unicorn_conf)})"

namespace :deploy do
  before :deploy, "deploy:check_revision"
  after :deploy, "deploy:restart"
  after :rollback, "deploy:restart"
end
