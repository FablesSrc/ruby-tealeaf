namespace :deploy do
  desc "Makes sure local git is in sync with remote."
  task :check_revision do
    unless `git rev-parse HEAD` == `git rev-parse origin/master`
      puts "WARNING: HEAD is not the same as origin/master"
      puts "Run `git push` to sync changes."
      exit
    end
  end

  # - for unicorn - #
  desc "Start application"
  task :start do
    on roles(:app) do |host|
      execute fetch(:unicorn_start_cmd)
    end
  end

  desc "Stop application"
  task :stop do
    on roles(:app) do |host|
      execute "[ -f #{fetch(:unicorn_pid)} ] && kill -QUIT `cat #{fetch(:unicorn_pid)}`"
    end
  end

  desc "Restart Application"
  task :restart do
    on roles(:app) do |host|
      execute "[ -f #{fetch(:unicorn_pid)} ] && kill -USR2 `cat #{fetch(:unicorn_pid)}` || #{fetch(:unicorn_start_cmd)}"
    end
  end
end