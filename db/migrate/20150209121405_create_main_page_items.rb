class CreateMainPageItems < ActiveRecord::Migration
  def change
    create_table :main_page_items do |t|
      t.string :title
      t.string :description
      t.attachment :image

      t.timestamps null: false
    end
  end
end
