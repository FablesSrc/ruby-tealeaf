class MainPageItemsController < ApplicationController
  before_action :set_main_page_item, only: [:show, :edit, :update, :destroy]

  # GET /main_page_items
  # GET /main_page_items.json
  def index
    @main_page_items = MainPageItem.all
  end

  # GET /main_page_items/1
  # GET /main_page_items/1.json
  def show
  end

  # GET /main_page_items/new
  def new
    @main_page_item = MainPageItem.new
  end

  # GET /main_page_items/1/edit
  def edit
  end

  # POST /main_page_items
  # POST /main_page_items.json
  def create
    @main_page_item = MainPageItem.new(main_page_item_params)

    respond_to do |format|
      if @main_page_item.save
        format.html { redirect_to @main_page_item, notice: 'Main page item was successfully created.' }
        format.json { render :show, status: :created, location: @main_page_item }
      else
        format.html { render :new }
        format.json { render json: @main_page_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /main_page_items/1
  # PATCH/PUT /main_page_items/1.json
  def update
    respond_to do |format|
      if @main_page_item.update(main_page_item_params)
        format.html { redirect_to @main_page_item, notice: 'Main page item was successfully updated.' }
        format.json { render :show, status: :ok, location: @main_page_item }
      else
        format.html { render :edit }
        format.json { render json: @main_page_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /main_page_items/1
  # DELETE /main_page_items/1.json
  def destroy
    @main_page_item.destroy
    respond_to do |format|
      format.html { redirect_to main_page_items_url, notice: 'Main page item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_main_page_item
      @main_page_item = MainPageItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def main_page_item_params
      params.require(:main_page_item).permit(:title, :description, :image)
    end
end
