require 'test_helper'

class MainPageItemsControllerTest < ActionController::TestCase
  setup do
    @main_page_item = main_page_items(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:main_page_items)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create main_page_item" do
    assert_difference('MainPageItem.count') do
      post :create, main_page_item: { description: @main_page_item.description, title: @main_page_item.title }
    end

    assert_redirected_to main_page_item_path(assigns(:main_page_item))
  end

  test "should show main_page_item" do
    get :show, id: @main_page_item
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @main_page_item
    assert_response :success
  end

  test "should update main_page_item" do
    patch :update, id: @main_page_item, main_page_item: { description: @main_page_item.description, title: @main_page_item.title }
    assert_redirected_to main_page_item_path(assigns(:main_page_item))
  end

  test "should destroy main_page_item" do
    assert_difference('MainPageItem.count', -1) do
      delete :destroy, id: @main_page_item
    end

    assert_redirected_to main_page_items_path
  end
end
